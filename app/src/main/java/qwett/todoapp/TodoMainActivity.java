package qwett.todoapp;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

import qwett.todoapp.Model.CovidStatusModel;
import qwett.todoapp.Model.SubTask;
import qwett.todoapp.Model.Task;
import qwett.todoapp.Request.CovidStatus;
import qwett.todoapp.Service.Task.ComparatorTaskId;
import qwett.todoapp.Service.Task.ComparatorTaskPriority;
import qwett.todoapp.Service.Task.ComparatorTaskPriorityReverse;
import qwett.todoapp.Service.Task.TaskAdapter;
import qwett.todoapp.Service.Task.TaskManager;
import qwett.todoapp.Service.Task.TaskManagerImpl;

public class TodoMainActivity extends AppCompatActivity implements TaskAdapter.MultipleClickListener, CovidStatus.AsyncResponse, PopupMenu.OnMenuItemClickListener {

    private RecyclerView taskRecyclerView;
    private TaskManager taskManager;
    private TaskAdapter taskAdapter;
    private boolean mState = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.todo_main_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        taskManager = TaskManagerImpl.getInstance(this);
        taskAdapter = new TaskAdapter(taskManager.getAllTask());
        taskAdapter.setOnItemClickListener(this);

        taskRecyclerView = findViewById(R.id.task_recycler_view);
        taskRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        taskRecyclerView.setAdapter(taskAdapter);

        new CovidStatus(this).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_menu, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        if (mState) {
            for (int i = 0; i < menu.size() - 2; i++)
                menu.getItem(i).setVisible(true);
        } else {
            for (int i = 0; i < menu.size() - 2; i++)
                menu.getItem(i).setVisible(false);
            menu.getItem(menu.size() - 1).setVisible(true);
            menu.getItem(menu.size() - 2).setVisible(true);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                taskAdapter.getFilter().filter(newText);
                return false;

            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                return true;
            case R.id.action_edit:
                Intent intent = new Intent(this, TaskActivity.class);
                this.startActivity(intent);
                return true;
            case R.id.action_remove:
                List<Task> taskList = taskAdapter.getSelectedItems();
                taskManager.removeTasks(taskList);
                taskAdapter.removeItem(taskList);
                mState = true;
                invalidateOptionsMenu();
                getSupportActionBar().setTitle("");
                taskAdapter.setItemRemovable(false);
                return true;
            case R.id.action_cancel:
                mState = true;
                invalidateOptionsMenu();
                getSupportActionBar().setTitle("");
                taskAdapter.setItemRemovable(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sortById:
                taskAdapter.sortItems(new ComparatorTaskId());
                return true;
            case R.id.sortByPriority:
                taskAdapter.sortItems(new ComparatorTaskPriority());
                return true;
            case R.id.sortByPriorityReverse:
                taskAdapter.sortItems(new ComparatorTaskPriorityReverse());
                return true;
            case R.id.category_all:
                List<Task> taskList = taskManager.getAllTask();
                taskAdapter.setItems(taskList);
                return true;
            case R.id.category_home:
                String category = getString(R.string.text_menu_category_home);
                taskList = taskManager.filterTaskByCategory(category);
                taskAdapter.setItems(taskList);
                return true;
            case R.id.category_work:
                category = getString(R.string.text_menu_category_work);
                taskList = taskManager.filterTaskByCategory(category);
                taskAdapter.setItems(taskList);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onItemClick(int position, View v) {

        String toolbarTitle = getString(R.string.selected) + ": " + taskAdapter.getCountSelectedItems();
        if (mState) {
            mState = false;
            invalidateOptionsMenu();
        }
        getSupportActionBar().setTitle(toolbarTitle);
    }

    @Override
    public void processFinish(CovidStatusModel output) {
        if (output != null) {
            TextView textConfirmedCount = findViewById(R.id.tv_confirmed_count);
            TextView textActiveCount = findViewById(R.id.tv_active_count);
            TextView textDeathsCount = findViewById(R.id.tv_deaths_count);
            TextView textRecoveredCount = findViewById(R.id.tv_recovered_count);

            textConfirmedCount.setText(Integer.toString(output.getConfirmed()));
            textActiveCount.setText(Integer.toString(output.getActive()));
            textDeathsCount.setText(Integer.toString(output.getDeaths()));
            textRecoveredCount.setText(Integer.toString(output.getRecovered()));
        }
    }

    public void showPopupForSort(MenuItem item) {
        View menuItemView = findViewById(R.id.action_sort);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.sort_context_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    public void showPopupForFilterCategory(MenuItem item) {
        View menuItemView = findViewById(R.id.action_filter_category);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.filter_category_context_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

}

