package qwett.todoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import qwett.todoapp.Model.ListTasks;
import qwett.todoapp.Model.SubTask;
import qwett.todoapp.Model.Task;
import qwett.todoapp.Service.Task.CreateTaskController;

public class TaskActivity extends Activity implements View.OnClickListener {


    private Map<Integer, ListTasks> allViewTask;
    private int counter = 0;
    private LinearLayout linear;
    private List<Integer> subCounter = new ArrayList<>();
    private CreateTaskController createTaskController;
    private EditText etTaskTitle;
    private EditText etTaskDescription;
    private Spinner spinnerPriority;
    private Spinner spinnerCategory;
    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_task_activity);
        createTaskController = new CreateTaskController();
        Button addButton = (Button) findViewById(R.id.button_add_task);
        Button saveButton = (Button) findViewById(R.id.button_save);
        Button updateButton = (Button) findViewById(R.id.button_change);
        etTaskTitle = (EditText) findViewById(R.id.subTaskActive);
        etTaskDescription = (EditText) findViewById(R.id.et_task_description);
        allViewTask = new HashMap<>();
        linear = (LinearLayout) findViewById(R.id.linear);
        addButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);

        spinnerPriority = (Spinner) findViewById(R.id.taskPriority);
        ArrayAdapter<CharSequence> adapterSpinnerPriority = ArrayAdapter.createFromResource(this,
                R.array.spinner_priority_array, android.R.layout.simple_spinner_item);
        adapterSpinnerPriority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPriority.setAdapter(adapterSpinnerPriority);

        spinnerCategory = (Spinner) findViewById(R.id.taskCategory);
        ArrayAdapter<CharSequence> adapterSpinnerCategory = ArrayAdapter.createFromResource(this,
                R.array.spinner_category_array, android.R.layout.simple_spinner_item);
        adapterSpinnerCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapterSpinnerCategory);


        task = (Task) getIntent().getSerializableExtra("task");
        if (task != null) {
            saveButton.setVisibility(View.GONE);
            updateButton.setVisibility(View.VISIBLE);
            updateButton.setOnClickListener(this);

            etTaskTitle.setText(task.getTaskTitle());
            etTaskDescription.setText(task.getTaskDescription());
            int spinnerPosition = adapterSpinnerPriority.getPosition(task.getTaskPriority());
            spinnerPriority.setSelection(spinnerPosition);

            spinnerPosition = adapterSpinnerCategory.getPosition(task.getTaskCategory());
            spinnerCategory.setSelection(spinnerPosition);
            addSubTask(task.getSubTaskList());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add_task:
                addSubTask();
                break;
            case R.id.button_change:
                task.setTaskTitle(etTaskTitle.getText().toString());
                task.setTaskDescription(etTaskDescription.getText().toString());
                String priority = String.valueOf(spinnerPriority.getSelectedItem());
                String category = String.valueOf(spinnerCategory.getSelectedItem());
                task.setTaskPriority(priority);
                task.setTaskCategory(category);
                createTaskController.updateTask(task, allViewTask);
                Intent intent = new Intent(this, TodoMainActivity.class);
                this.startActivity(intent);
                this.finish();
                break;
            case R.id.button_save:
                priority = String.valueOf(spinnerPriority.getSelectedItem());
                category = String.valueOf(spinnerCategory.getSelectedItem());
                createTaskController.addNewTask(etTaskTitle.getText().toString(), etTaskDescription.getText().toString(), priority, category, allViewTask);
                intent = new Intent(this, TodoMainActivity.class);
                this.startActivity(intent);
                this.finish();
                break;
        }
    }

    private void addSubTask() {
        final View view = getLayoutInflater().inflate(R.layout.custom_edittext_layout, null);
        final LinearLayout linear_subtask = (LinearLayout) view.findViewById(R.id.linear_subtask);
        final int index = counter;
        subCounter.add(0);
        counter++;
        Button deleteField = (Button) view.findViewById(R.id.button_delete_task);
        deleteField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view.getParent() != null) {
                    ((LinearLayout) view.getParent()).removeView(view);
                    allViewTask.remove(index);
                }
            }
        });

        EditText text = (EditText) view.findViewById(R.id.subTaskActive);
        String textHint = getString(R.string.text_add_task_hint);
        text.setHint(textHint + " " + counter);

        ListTasks listTasks = new ListTasks(view);
        allViewTask.put(index, listTasks);
        linear.addView(view);


        Button subtaskField = (Button) view.findViewById(R.id.button_subtask);
        subtaskField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View viewSubTask = getLayoutInflater().inflate(R.layout.custom_edittext_layout, null);
                subCounter.set(index, subCounter.get(index) + 1);
                viewSubTask.findViewById(R.id.button_subtask).setVisibility(View.GONE);
                viewSubTask.findViewById(R.id.textView).setVisibility(View.GONE);
                Button subDeleteField = (Button) viewSubTask.findViewById(R.id.button_delete_task);
                subDeleteField.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewSubTask.getParent() != null) {
                            ((LinearLayout) viewSubTask.getParent()).removeView(viewSubTask);
                            listTasks.removeSubTaskView(viewSubTask);
                        }
                    }
                });

                EditText text = (EditText) viewSubTask.findViewById(R.id.subTaskActive);
                String textHint = getString(R.string.text_task_hint);
                text.setHint(textHint + " " + subCounter.get(index));
                listTasks.addSubTaskView(viewSubTask);
                linear_subtask.addView(viewSubTask);
            }
        });
    }

    private void addSubTask(List<SubTask> mainSubTaskList) {
        if (mainSubTaskList != null)
            for (SubTask eMainSubTask : mainSubTaskList) {
                final View view = getLayoutInflater().inflate(R.layout.custom_edittext_layout, null);
                final LinearLayout linear_subtask = (LinearLayout) view.findViewById(R.id.linear_subtask);
                final int index = counter;
                CheckBox checkBox = view.findViewById(R.id.checkBox);
                if (eMainSubTask.isSelected())
                    checkBox.setChecked(true);

                subCounter.add(0);
                counter++;
                Button deleteField = (Button) view.findViewById(R.id.button_delete_task);
                deleteField.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (view.getParent() != null) {
                            ((LinearLayout) view.getParent()).removeView(view);
                            allViewTask.remove(index);
                        }
                    }
                });

                EditText text = (EditText) view.findViewById(R.id.subTaskActive);
                text.setText(eMainSubTask.getSubTaskTitle());

                ListTasks listTasks = new ListTasks(view);
                allViewTask.put(index, listTasks);
                linear.addView(view);


                Button subtaskField = (Button) view.findViewById(R.id.button_subtask);
                subtaskField.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final View viewSubTask = getLayoutInflater().inflate(R.layout.custom_edittext_layout, null);
                        subCounter.set(index, subCounter.get(index) + 1);
                        viewSubTask.findViewById(R.id.button_subtask).setVisibility(View.GONE);
                        viewSubTask.findViewById(R.id.textView).setVisibility(View.GONE);

                        Button subDeleteField = (Button) viewSubTask.findViewById(R.id.button_delete_task);
                        subDeleteField.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (viewSubTask.getParent() != null) {
                                    ((LinearLayout) viewSubTask.getParent()).removeView(viewSubTask);
                                    listTasks.removeSubTaskView(viewSubTask);
                                }
                            }
                        });

                        EditText text = (EditText) viewSubTask.findViewById(R.id.subTaskActive);
                        String textHint = getString(R.string.text_task_hint);
                        text.setHint(textHint + " " + subCounter.get(index));
                        listTasks.addSubTaskView(viewSubTask);
                        linear_subtask.addView(viewSubTask);
                    }
                });

                if (eMainSubTask.getSubTaskList() != null && eMainSubTask.getSubTaskList().size() != 0)
                    for (SubTask eSubTask : eMainSubTask.getSubTaskList()) {
                        final View viewSubTask = getLayoutInflater().inflate(R.layout.custom_edittext_layout, null);
                        subCounter.set(index, subCounter.get(index) + 1);
                        viewSubTask.findViewById(R.id.button_subtask).setVisibility(View.GONE);
                        viewSubTask.findViewById(R.id.textView).setVisibility(View.GONE);

                        CheckBox subCheckBox = viewSubTask.findViewById(R.id.checkBox);
                        if (eSubTask.isSelected())
                            subCheckBox.setChecked(true);

                        Button subDeleteField = (Button) viewSubTask.findViewById(R.id.button_delete_task);
                        subDeleteField.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (viewSubTask.getParent() != null) {
                                    ((LinearLayout) viewSubTask.getParent()).removeView(viewSubTask);
                                    listTasks.removeSubTaskView(viewSubTask);
                                }
                            }
                        });

                        EditText textSubTask = (EditText) viewSubTask.findViewById(R.id.subTaskActive);
                        textSubTask.setText(eSubTask.getSubTaskTitle());
                        listTasks.addSubTaskView(viewSubTask);
                        linear_subtask.addView(viewSubTask);
                    }
            }
    }

}
