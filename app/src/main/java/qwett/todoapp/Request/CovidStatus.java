package qwett.todoapp.Request;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import qwett.todoapp.Model.CovidStatusModel;

public class CovidStatus extends AsyncTask<Void, Void, CovidStatusModel> {

    private AsyncResponse delegate = null;

    public CovidStatus(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    public interface AsyncResponse {
        void processFinish(CovidStatusModel output);
    }


    @Override
    protected CovidStatusModel doInBackground(Void... voids) {
        Calendar calendarTo = Calendar.getInstance(); // this would default to now
        Calendar calendarFrom = Calendar.getInstance(); // this would default to now
        calendarFrom.add(Calendar.DAY_OF_MONTH, -7);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        String dateFrom = df.format(calendarFrom.getTime());
        String dateTo = df.format(calendarTo.getTime());

        String url = "https://api.covid19api.com/country/russia?" + "from=" + dateFrom + "Z&to=" + dateTo;

        CovidStatusModel covidStatusModel = new CovidStatusModel();
        Type listType = new TypeToken<List<CovidStatusModel>>() {
        }.getType();
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .build();
        try {
            Response response = client.newCall(request).execute();
            String myResponse = response.body().string();
            Gson gson = new Gson();
            List<CovidStatusModel> covidStatusModels = gson.fromJson(myResponse, listType);
            covidStatusModel = covidStatusModels.get(covidStatusModels.size() - 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return covidStatusModel;
    }

    @Override
    protected void onPostExecute(CovidStatusModel covidStatusModel) {
        super.onPostExecute(covidStatusModel);
        delegate.processFinish(covidStatusModel);
    }

}
