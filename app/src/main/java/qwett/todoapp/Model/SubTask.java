package qwett.todoapp.Model;

import java.io.Serializable;
import java.util.List;

public class SubTask implements Serializable {

    private String idSubTask;
    private String subTaskTitle;

    private List<SubTask> subTaskList;
    private boolean isSelected;

    public SubTask(String subTaskTitle, boolean isSelected) {
        this.subTaskTitle = subTaskTitle;
        this.isSelected = isSelected;
    }

    public SubTask(String idSubTask, String subTaskTitle, boolean isSelected) {
        this.idSubTask = idSubTask;
        this.subTaskTitle = subTaskTitle;
        this.isSelected = isSelected;
    }

    public SubTask(String idSubTask, String subTaskTitle, List<SubTask> subTaskList, boolean isSelected) {
        this.idSubTask = idSubTask;
        this.subTaskTitle = subTaskTitle;
        this.subTaskList = subTaskList;
        this.isSelected = isSelected;
    }

    public SubTask(String subTaskTitle, List<SubTask> subTaskList, boolean isSelected) {
        this.subTaskTitle = subTaskTitle;
        this.subTaskList = subTaskList;
        this.isSelected = isSelected;
    }

    public String getIdSubTask() {
        return idSubTask;
    }

    public void setIdSubTask(String idSubTask) {
        this.idSubTask = idSubTask;
    }

    public String getSubTaskTitle() {
        return subTaskTitle;
    }

    public void setSubTaskTitle(String subTaskTitle) {
        this.subTaskTitle = subTaskTitle;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public List<SubTask> getSubTaskList() {
        return subTaskList;
    }

    public void setSubTaskList(List<SubTask> subTaskList) {
        this.subTaskList = subTaskList;
    }


}
