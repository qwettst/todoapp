package qwett.todoapp.Model;

import java.io.Serializable;
import java.util.List;

public class Task implements Serializable {
    private int taskId;
    private int positionView;
    private String taskCategory;
    private String taskTitle;
    private String taskDescription;
    private String taskPriority;
    private String subTaskId;
    private boolean isSelected = false;
    private List<SubTask> subTaskList;

    public Task(int taskId, String taskTitle, String taskDescription, String taskCategory, String taskPriority, String subTaskId, List<SubTask> subTaskList) {
        this.taskId = taskId;
        this.taskTitle = taskTitle;
        this.taskDescription = taskDescription;
        this.taskCategory = taskCategory;
        this.taskPriority = taskPriority;
        this.subTaskId = subTaskId;
        this.subTaskList = subTaskList;
    }

    public Task(String taskTitle, String taskDescription, String taskPriority, String taskCategory, List<SubTask> subTaskList) {
        this.taskTitle = taskTitle;
        this.taskDescription = taskDescription;
        this.taskPriority = taskPriority;
        this.taskCategory = taskCategory;
        this.subTaskList = subTaskList;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(String taskPriority) {
        this.taskPriority = taskPriority;
    }

    public String getSubTaskId() {
        return subTaskId;
    }

    public void setSubTaskId(String subTaskId) {
        this.subTaskId = subTaskId;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public List<SubTask> getSubTaskList() {
        return subTaskList;
    }

    public void setSubTaskList(List<SubTask> subTaskList) {
        this.subTaskList = subTaskList;
    }

    public int getPositionView() {
        return positionView;
    }

    public void setPositionView(int positionView) {
        this.positionView = positionView;
    }


    public String getTaskCategory() {
        return taskCategory;
    }

    public void setTaskCategory(String taskCategory) {
        this.taskCategory = taskCategory;
    }
}
