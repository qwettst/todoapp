package qwett.todoapp.Model;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class ListTasks {

    private View mainTask;
    private List<View> subTask;

    public ListTasks(View mainTask) {
        this.mainTask = mainTask;
        subTask = new ArrayList<>();
    }

    public View getMainTask() {
        return mainTask;
    }

    public void setMainTask(View mainTask) {
        this.mainTask = mainTask;
    }

    public List<View> getSubTask() {
        return subTask;
    }

    public void setSubTask(List<View> subTask) {
        this.subTask = subTask;
    }

    public void addSubTaskView(View v) {
        subTask.add(v);
    }

    public void removeSubTaskView(View v) {
        subTask.remove(v);
    }

}
