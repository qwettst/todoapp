package qwett.todoapp.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import qwett.todoapp.Model.SubTask;
import qwett.todoapp.Model.Task;

public class SqlLocalStorage extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "taskDB.db";

    private static final int DATABASE_VERSION = 5;

    public SqlLocalStorage(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_TASK_TABLE = "CREATE TABLE " + TaskDBContract.TaskDB.TABLE_NAME + " ("
                + TaskDBContract.TaskDB._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TaskDBContract.TaskDB.COLUMN_SUBTASK_ID + " INTEGER, "
                + TaskDBContract.TaskDB.COLUMN_TASK_TITLE + " TEXT NOT NULL, "
                + TaskDBContract.TaskDB.COLUMN_TASK_DESCRIPTION + " TEXT, "
                + TaskDBContract.TaskDB.COLUMN_TASK_CATEGORY + " TEXT, "
                + TaskDBContract.TaskDB.COLUMN_TASK_PRIORITY + " INTEGER NOT NULL DEFAULT 1);";

        String SQL_CREATE_SUBTASK_TABLE = "CREATE TABLE " + TaskDBContract.SubTaskDB.TABLE_NAME + " ("
                + TaskDBContract.SubTaskDB._ID + " TEXT NOT NULL, "
                + TaskDBContract.SubTaskDB.COLUMN_TASK_TITLE + " TEXT NOT NULL, "
                + TaskDBContract.SubTaskDB.COLUMN_IS_SELECTED + " INTEGER NOT NULL DEFAULT 0);";

        db.execSQL(SQL_CREATE_TASK_TABLE);
        db.execSQL(SQL_CREATE_SUBTASK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Запишем в журнал
        Log.w("SQLite", "Обновляемся с версии " + oldVersion + " на версию " + newVersion);
        // Удаляем старую таблицу и создаём новую
        db.execSQL("DROP TABLE IF EXISTS " + TaskDBContract.TaskDB.TABLE_NAME);
        // Создаём новую таблицу
        db.execSQL("DROP TABLE IF EXISTS " + TaskDBContract.SubTaskDB.TABLE_NAME);
        onCreate(db);
    }

    public void updateTask(Task task) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(TaskDBContract.TaskDB.COLUMN_TASK_TITLE, task.getTaskTitle());
        values.put(TaskDBContract.TaskDB.COLUMN_TASK_DESCRIPTION, task.getTaskDescription());
        values.put(TaskDBContract.TaskDB.COLUMN_TASK_PRIORITY, task.getTaskPriority());
        values.put(TaskDBContract.TaskDB.COLUMN_TASK_CATEGORY, task.getTaskCategory());
        values.put(TaskDBContract.TaskDB.COLUMN_SUBTASK_ID, task.getSubTaskId());
        db.update(TaskDBContract.TaskDB.TABLE_NAME, values, TaskDBContract.TaskDB._ID + " = " + task.getTaskId(), null);
        if (task.getSubTaskList() != null && task.getSubTaskList().size() != 0) {
            updateSubTask(task.getSubTaskList(), task.getSubTaskId(), db);
        }
        db.close();
    }

    public void updateSubTask(List<SubTask> subTaskList, String idSubTask, SQLiteDatabase db) {
        removeSubTasks(idSubTask, db);
        addSubTask(subTaskList, idSubTask, db);
    }

    public List<Task> getAllTasks() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Task> taskList = new ArrayList<>();

        String[] projection = {
                TaskDBContract.TaskDB._ID,
                TaskDBContract.TaskDB.COLUMN_SUBTASK_ID,
                TaskDBContract.TaskDB.COLUMN_TASK_TITLE,
                TaskDBContract.TaskDB.COLUMN_TASK_DESCRIPTION,
                TaskDBContract.TaskDB.COLUMN_TASK_PRIORITY,
                TaskDBContract.TaskDB.COLUMN_TASK_CATEGORY};

        Cursor cursor = db.query(
                TaskDBContract.TaskDB.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null);

        int idColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB._ID);
        int idSubTaskColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_SUBTASK_ID);
        int taskTitleColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_TASK_TITLE);
        int taskDescriptionColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_TASK_DESCRIPTION);
        int taskPriorityColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_TASK_PRIORITY);
        int taskCategoryColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_TASK_CATEGORY);

        try {
            while (cursor.moveToNext()) {
                int idTask = cursor.getInt(idColumnIndex);
                String idSubTask = cursor.getString(idSubTaskColumnIndex);
                String taskTitle = cursor.getString(taskTitleColumnIndex);
                String taskDescription = cursor.getString(taskDescriptionColumnIndex);
                String taskPriority = cursor.getString(taskPriorityColumnIndex);
                String taskCategory = cursor.getString(taskCategoryColumnIndex);

                String strIdSubTask = Long.toString(idTask) + '_' + Long.toString(idTask);
                List<SubTask> subTaskList = getAllSubTasks(strIdSubTask);

                Task task = new Task(idTask, taskTitle, taskDescription, taskCategory, taskPriority, idSubTask, subTaskList);
                taskList.add(task);
            }
        } finally {
            cursor.close();
        }

        db.close();
        return taskList;
    }

    public List<Task> getFilteredTaskByCategory(String category) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Task> taskList = new ArrayList<>();

        String[] projection = {
                TaskDBContract.TaskDB._ID,
                TaskDBContract.TaskDB.COLUMN_SUBTASK_ID,
                TaskDBContract.TaskDB.COLUMN_TASK_TITLE,
                TaskDBContract.TaskDB.COLUMN_TASK_DESCRIPTION,
                TaskDBContract.TaskDB.COLUMN_TASK_PRIORITY,
                TaskDBContract.TaskDB.COLUMN_TASK_CATEGORY};

        String where = TaskDBContract.TaskDB.COLUMN_TASK_CATEGORY + " = ? ";
        String[] args = {category};

        Cursor cursor = db.query(
                TaskDBContract.TaskDB.TABLE_NAME,
                projection,
                where,
                args,
                null,
                null,
                null);

        int idColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB._ID);
        int idSubTaskColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_SUBTASK_ID);
        int taskTitleColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_TASK_TITLE);
        int taskDescriptionColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_TASK_DESCRIPTION);
        int taskPriorityColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_TASK_PRIORITY);
        int taskCategoryColumnIndex = cursor.getColumnIndex(TaskDBContract.TaskDB.COLUMN_TASK_CATEGORY);

        try {
            while (cursor.moveToNext()) {
                int idTask = cursor.getInt(idColumnIndex);
                String idSubTask = cursor.getString(idSubTaskColumnIndex);
                String taskTitle = cursor.getString(taskTitleColumnIndex);
                String taskDescription = cursor.getString(taskDescriptionColumnIndex);
                String taskPriority = cursor.getString(taskPriorityColumnIndex);
                String taskCategory = cursor.getString(taskCategoryColumnIndex);

                String strIdSubTask = Long.toString(idTask) + '_' + Long.toString(idTask);
                List<SubTask> subTaskList = getAllSubTasks(strIdSubTask);

                Task task = new Task(idTask, taskTitle, taskDescription, taskCategory, taskPriority, idSubTask, subTaskList);
                taskList.add(task);
            }
        } finally {
            cursor.close();
        }

        db.close();
        return taskList;
    }

    public void addTask(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TaskDBContract.TaskDB.COLUMN_TASK_TITLE, task.getTaskTitle());
        values.put(TaskDBContract.TaskDB.COLUMN_TASK_DESCRIPTION, task.getTaskDescription());
        values.put(TaskDBContract.TaskDB.COLUMN_TASK_PRIORITY, task.getTaskPriority());
        values.put(TaskDBContract.TaskDB.COLUMN_TASK_CATEGORY, task.getTaskCategory());
        Long newRowId = db.insert(TaskDBContract.TaskDB.TABLE_NAME, null, values);
        String idSubTask = Long.toString(newRowId) + '_' + Long.toString(newRowId);
        if (task.getSubTaskList() != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(TaskDBContract.TaskDB.COLUMN_SUBTASK_ID, idSubTask);
            addSubTask(task.getSubTaskList(), idSubTask, db);
            db.update(TaskDBContract.TaskDB.TABLE_NAME, contentValues, TaskDBContract.TaskDB._ID + " = " + newRowId, null);
        }
        db.close();
    }

    public void addSubTask(List<SubTask> subTaskList, String idSubTask, SQLiteDatabase db) {
        int index = 0;
        for (SubTask e : subTaskList) {
            ContentValues values = new ContentValues();
            values.put(TaskDBContract.SubTaskDB._ID, idSubTask);
            values.put(TaskDBContract.SubTaskDB.COLUMN_TASK_TITLE, e.getSubTaskTitle());
            values.put(TaskDBContract.SubTaskDB.COLUMN_IS_SELECTED, e.isSelected());
            db.insert(TaskDBContract.SubTaskDB.TABLE_NAME, null, values);
            if (e.getSubTaskList() != null && e.getSubTaskList().size() != 0) {
                String id2ndTask = idSubTask + '_' + Integer.toString(index);
                addSubTask(e.getSubTaskList(), id2ndTask, db);
            }
            index++;
        }
    }

    public void removeTasks(Collection<Task> taskList) {
        SQLiteDatabase db = this.getWritableDatabase();
        String where = TaskDBContract.TaskDB._ID + " = ";
        for (Task e : taskList) {
            if (e.getSubTaskId() != null) {
                removeSubTasks(e.getSubTaskId(), db);
            }
            db.delete(TaskDBContract.TaskDB.TABLE_NAME, where + e.getTaskId(), null);
        }
        db.close();
    }

    public void removeSubTasks(String strIdSubTask, SQLiteDatabase db) {
        String where = TaskDBContract.SubTaskDB._ID + " LIKE '" + strIdSubTask + "%'";
        db.delete(TaskDBContract.SubTaskDB.TABLE_NAME, where, null);
    }

    private List<SubTask> getAllSubTasks(String strIdSubTask) {
        List<SubTask> subTaskList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                TaskDBContract.SubTaskDB._ID,
                TaskDBContract.SubTaskDB.COLUMN_TASK_TITLE,
                TaskDBContract.SubTaskDB.COLUMN_IS_SELECTED};

        String where = TaskDBContract.SubTaskDB._ID + " = ? ";
        String[] args = {strIdSubTask};
        Cursor cursor = db.query(
                TaskDBContract.SubTaskDB.TABLE_NAME,
                projection,
                where,
                args,
                null,
                null,
                null);
        int idColumnIndex = cursor.getColumnIndex(TaskDBContract.SubTaskDB._ID);
        int subTaskColumnIndex = cursor.getColumnIndex(TaskDBContract.SubTaskDB.COLUMN_TASK_TITLE);
        int subTaskIsSelectedColumnIndex = cursor.getColumnIndex(TaskDBContract.SubTaskDB.COLUMN_IS_SELECTED);
        int index = 0;
        try {
            while (cursor.moveToNext()) {
                String idSubTask = cursor.getString(idColumnIndex);
                String subTaskTitle = cursor.getString(subTaskColumnIndex);
                boolean subTaskIsSelected = (cursor.getInt(subTaskIsSelectedColumnIndex) == 1);
                SubTask subTask = new SubTask(idSubTask, subTaskTitle, subTaskIsSelected);
                subTask.setSubTaskList(getAllSubTasks(strIdSubTask + '_' + Integer.toString(index)));
                subTaskList.add(subTask);
                index++;
            }
        } finally {
            cursor.close();
        }

        db.close();
        return subTaskList;
    }
}
