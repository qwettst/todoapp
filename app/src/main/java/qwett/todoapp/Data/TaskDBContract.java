package qwett.todoapp.Data;

import android.provider.BaseColumns;

public final class TaskDBContract {
    private TaskDBContract() {

    }

    public static final class TaskDB implements BaseColumns {
        public final static String TABLE_NAME = "MainTask";
        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_TASK_TITLE = "taskTitle";
        public final static String COLUMN_TASK_DESCRIPTION = "taskDescription";
        public final static String COLUMN_TASK_PRIORITY = "taskPriority";
        public final static String COLUMN_TASK_CATEGORY = "taskCategory";
        public final static String COLUMN_SUBTASK_ID = "subTaskID";
    }

    public static final class SubTaskDB implements BaseColumns {
        public final static String TABLE_NAME = "SubTask";
        public final static String _ID = "IdSubTask";
        public final static String COLUMN_TASK_TITLE = "taskTitle";
        public final static String COLUMN_IS_SELECTED = "isSelected";
    }

}
