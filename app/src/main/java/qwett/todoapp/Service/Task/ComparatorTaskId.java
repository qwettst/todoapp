package qwett.todoapp.Service.Task;

import java.util.Comparator;

import qwett.todoapp.Model.Task;

public class ComparatorTaskId implements Comparator<Task> {
    @Override
    public int compare(Task o1, Task o2) {
        int p1 = o1.getTaskId();
        int p2 = o2.getTaskId();
        return p1 < p2 ? -1 : p1 == p2 ? 0 : 1;
    }
}
