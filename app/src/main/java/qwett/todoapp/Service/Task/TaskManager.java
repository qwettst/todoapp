package qwett.todoapp.Service.Task;

import java.util.List;

import qwett.todoapp.Model.Task;

public interface TaskManager {
    void addTask(Task task);

    void removeTasks(List<Task> taskList);

    List<Task> filterTaskByCategory(String category);

    void updateTask(Task task);

    List<Task> getAllTask();
}
