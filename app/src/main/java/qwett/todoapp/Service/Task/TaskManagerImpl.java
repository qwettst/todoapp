package qwett.todoapp.Service.Task;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import qwett.todoapp.Data.SqlLocalStorage;
import qwett.todoapp.Model.SubTask;
import qwett.todoapp.Model.Task;

public class TaskManagerImpl implements TaskManager {


    private static TaskManagerImpl instance;
    private List<Task> tasks = new ArrayList<>();
    private SqlLocalStorage sqlLocalStorage;


    private TaskManagerImpl(Context context) {
        sqlLocalStorage = new SqlLocalStorage(context);
    }

    public static synchronized TaskManagerImpl getInstance(Context context) {
        if (instance == null) {
            instance = new TaskManagerImpl(context);
        }
        return instance;
    }

    public static TaskManagerImpl getInstance() {
        return instance;
    }


    @Override
    public void addTask(Task task) {
        sqlLocalStorage.addTask(task);
    }


    @Override
    public void removeTasks(List<Task> taskList) {
        sqlLocalStorage.removeTasks(taskList);
    }

    @Override
    public List<Task> filterTaskByCategory(String category) {
        return sqlLocalStorage.getFilteredTaskByCategory(category);
    }

    @Override
    public void updateTask(Task task) {
        sqlLocalStorage.updateTask(task);
    }


    @Override
    public List<Task> getAllTask() {
        return sqlLocalStorage.getAllTasks();
    }


}
