package qwett.todoapp.Service.Task;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import qwett.todoapp.Model.ListTasks;
import qwett.todoapp.Model.SubTask;
import qwett.todoapp.Model.Task;
import qwett.todoapp.R;

public class CreateTaskController {
    private TaskManager taskManager;

    public CreateTaskController() {
        taskManager = TaskManagerImpl.getInstance();
    }

    public void addNewTask(String taskTitle, String taskDescription, String taskPriority, String taskCategory, Map<Integer, ListTasks> allViewTask) {
        Task task = setData(taskTitle, taskDescription, taskPriority, taskCategory, allViewTask);
        taskManager.addTask(task);
    }

    public void updateTask(Task task, Map<Integer, ListTasks> allViewTask) {
        Task newTask = setData(task.getTaskTitle(), task.getTaskDescription(), task.getTaskPriority(), task.getTaskCategory(), allViewTask);
        newTask.setTaskId(task.getTaskId());
        newTask.setSubTaskId(task.getSubTaskId());
        taskManager.updateTask(newTask);
    }

    private Task setData(String taskTitle, String taskDescription, String taskPriority, String taskCategory, Map<Integer, ListTasks> allViewTask) {
        String titleTask;
        CheckBox checkBoxTask;

        String titleSubTask;
        CheckBox checkBoxSubTask;

        List<SubTask> taskList = new ArrayList<>();
        SubTask subTask;
        for (Map.Entry<Integer, ListTasks> entry : allViewTask.entrySet()) {
            ListTasks eTask = entry.getValue();
            titleTask = ((EditText) eTask.getMainTask().findViewById(R.id.subTaskActive)).getText().toString();
            checkBoxTask = (CheckBox) eTask.getMainTask().findViewById(R.id.checkBox);
            boolean selectedTask = false;
            if (checkBoxTask.isChecked())
                selectedTask = true;

            if (eTask.getSubTask() != null) {
                List<SubTask> subTaskList = new ArrayList<>();
                for (View e : eTask.getSubTask()) {
                    titleSubTask = ((EditText) e.findViewById(R.id.subTaskActive)).getText().toString();
                    checkBoxSubTask = (CheckBox) e.findViewById(R.id.checkBox);
                    boolean selectedSubTask = false;
                    if (checkBoxSubTask.isChecked())
                        selectedSubTask = true;
                    SubTask tmpSubTask = new SubTask(titleSubTask, selectedSubTask);
                    subTaskList.add(tmpSubTask);
                }
                subTask = new SubTask(titleTask, subTaskList, selectedTask);
            } else {
                subTask = new SubTask(titleTask, selectedTask);
            }

            taskList.add(subTask);
        }
        Task task = new Task(taskTitle, taskDescription, taskPriority, taskCategory, taskList);
        return task;
    }

}
