package qwett.todoapp.Service.Task;

import java.util.Comparator;

import qwett.todoapp.Model.Task;

public class ComparatorTaskPriorityReverse implements Comparator<Task> {
    @Override
    public int compare(Task o1, Task o2) {
        int p1 = getIntPriority(o1.getTaskPriority());
        int p2 = getIntPriority(o2.getTaskPriority());
        return p1 < p2 ? -1 : p1 == p2 ? 0 : 1;
    }

    private int getIntPriority(String priority) {
        switch (priority) {
            case ("Высокий"):
                return 2;
            case ("Обычный"):
                return 1;
            case ("Низкий"):
                return 0;
        }
        return 1;
    }
}
