package qwett.todoapp.Service.Task;

import java.util.Comparator;

import qwett.todoapp.Model.Task;

public class ComparatorTaskTitle implements Comparator<Task> {
    @Override
    public int compare(Task o1, Task o2) {
        return o1.getTaskTitle().compareToIgnoreCase(o2.getTaskTitle());
    }
}
